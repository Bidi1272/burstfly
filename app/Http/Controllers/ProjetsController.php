<?php
    namespace App\Http\Controllers;
    use App\Http\Modeles\Projet as ProjetsMdl;
    use DB;
    use Illuminate\Support\Facades\View;

    class ProjetsController extends Controller{
        public function index($id = null){

            if($id):
                $projets = ProjetsMdl::join('categories', 'categories_id', '=', 'categories.id')
                                    ->select('projets.*', 'categories.*')
                                    ->where('categories_id', $id)
                                    ->get();
            else:
                $projets = ProjetsMdl::join('categories', 'categories_id', '=', 'categories.id')
                                    ->select('projets.*', 'categories.*')->get();
            endif;

            // AFFICHAGE DU RETURN DE LA QUERRY EN CAS D'ERREUR
                /*foreach ($projets as $projet) {
                    var_dump($projet);
                    echo "<hr />";
                    var_dump($projet->categories);
                    echo "<hr />";
                    dd($projet);
                }*/
            return View::make('projets.index', ['projets' => $projets]);
        }

        public function show($id = 1){
            $projet = ProjetsMdl::join('categories', 'categories_id', '=', 'categories.id')
                                ->select('projets.*', 'categories.*')
                                ->where('projets.id', $id)
                                ->first();
            $autres = ProjetsMdl::join('categories', 'categories_id', '=', 'categories.id')
                                ->select('projets.*', 'categories.*')
                                ->where('categories_id', $id)
                                ->limit(4)
                                ->get();


            /*var_dump($projet);
            echo "<hr />";
            var_dump($projet);
            echo "<hr />";
            dd($projet->titre);*/


            return View::make('projets.show', ['projet' => $projet, 'autres' => $autres]);
        }

    }
