<?php
    namespace App\Http\Modeles;
    use Illuminate\Database\Eloquent\Model;

    class Categorie extends Model{
        protected $table = 'categories';
    }
