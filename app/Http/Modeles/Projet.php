<?php
    namespace App\Http\Modeles;
    use Illuminate\Database\Eloquent\Model;

    class Projet extends Model{
        protected $table = 'projets';
        public function categorie()
        {
            return $this->belongsTo('App\Categorie');
            return $this->hasMany('App\Categorie');
        }
    }
