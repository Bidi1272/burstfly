@extends('templates.app')
@section('titre')
    Free grid work
@stop
@section('contenu')
    	<div class="container object">
    		<div id="main-container-image">
    		    <section class="work">
                    @foreach ($projets as $projet)
                        <figure class="white">
                            <a href="{{ URL::route('projet', ['id' => $projet->id]) }}">
                                <img src="{{ asset('img/'. $projet->image ) }}" alt="" />
                                <dl>
                                    <dt>{{ $projet->titre}}</dt>
                                    <dd>{{ $projet->description}}</dd>
                                </dl>
                            </a>
                            <div id="wrapper-part-info">
                                <div class="part-info-image"><img src="{{ asset('img/'. $projet->icone ) }}" alt="" width="28" height="28"/></div>
                                <div id="part-info">{{ $projet->titre }}</div>
                            </div>
                        </figure>
                    @endforeach
                </section>
            </div>
        </div>
@stop
