@foreach ($categories as $categorie)
    <div id="bouton-ai">
        <a href="{{ URL::route('tri', ['id' => $categorie->id]) }}">
            <img src="{{ asset('img/'.$categorie->icone ) }}" alt="{{ $categorie->slug }}" title="{{ $categorie->nom }}" height="28" width="28" />
        </a>
    </div>
@endforeach
