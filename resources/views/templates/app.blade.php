{{--
Template General
ressources/views/templates/app.blade.php
--}}
<!DOCTYPE HTML>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Platz - @yield('titre')</title>

        <!-- Behavioral Meta Data -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="icon" type="image/png" href="img/small-logo-01.png">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,900,900italic,700italic,700,500italic,400italic,500,300italic,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='{{ asset('css/style.css') }}' rel='stylesheet' type='text/css'>
        <link href='{{ asset('css/app2.css') }}' rel='stylesheet' type='text/css'>



    </head>

    <body>

        <div id="wrapper-container">

            <a name="ancre"></a>

            <!-- CACHE -->
            <div class="cache"></div>

            <!-- HEADER -->
            @include('templates.partials.header')


            <!-- NAVBAR -->

            @include('templates.partials.nav')

            <!-- FILTER -->

            @include('templates.partials.filtre')




            <!-- PORTFOLIO -->

            @section('contenu')
            @show


            @include('templates.partials.pagination')

            @include('templates.partials.footer')




        </div>



        <!-- SCRIPT -->
        @include('templates.partials.scripts')

    </body>


</html>
