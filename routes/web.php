<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

View::composer('categories.index', function($view){
    $view->with('categories', App\Http\Modeles\Categorie::all());
});


Route::get('/', 'ProjetsController@index')->name('accueil');
Route::get('/page/{id}', 'ProjetsController@index')->name('tri');
Route::get('/projet/{id}', 'ProjetsController@show')->name('projet');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
