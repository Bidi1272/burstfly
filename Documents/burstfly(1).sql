-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 20 juin 2018 à 14:41
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `burstfly`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `image`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Premium', 'icon-premium.svg', 'premium', NULL, NULL),
(2, 'Photo', 'icon-photo.svg', 'photo', NULL, NULL),
(3, 'Font', 'icon-font.svg', 'font', NULL, NULL),
(4, 'Thème', 'icon-theme.svg', 'theme', NULL, NULL),
(5, 'Photoshop', 'icon-photoshop.svg', 'photoshop', NULL, NULL),
(6, 'Illustrator', 'icon-illustrator.svg', 'illustrator', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `texte` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `projets_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `commentaires`
--

INSERT INTO `commentaires` (`id`, `nom`, `texte`, `created_at`, `projets_id`, `updated_at`) VALUES
(31, 'Roger', 'Très beau récit, très touchant', '2018-06-16 09:19:29', 1, NULL),
(32, 'Albert', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-17 12:20:00', 4, '2018-06-18 05:49:57'),
(33, 'Bernard', 'On ne peut pas parler de politique administrative scientifique, je vous en prie.', '2018-06-15 12:20:36', 3, NULL),
(34, 'Jordan', 'La politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-06 08:28:14', 4, NULL),
(35, 'Bryan', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, bonne année.', '2018-06-01 14:18:18', 5, NULL),
(36, 'Kevin', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-14 21:09:18', 6, NULL),
(37, 'Tangui', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-04 06:59:27', 7, NULL),
(38, 'Bertrand', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-07 10:08:14', 8, NULL),
(39, 'Hubert', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-01 09:20:30', 9, NULL),
(40, 'Gérard', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-20 09:22:00', 1, '2018-06-18 05:48:55'),
(41, 'Roger', 'Très beau récit, très touchant', '2018-06-16 09:19:00', 5, '2018-06-18 05:50:05'),
(42, 'Albert', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-17 12:20:00', 1, '2018-06-18 05:50:15'),
(43, 'Bernard', 'On ne peut pas parler de politique administrative scientifique, je vous en prie.', '2018-06-15 12:20:00', 2, '2018-06-18 05:50:34'),
(44, 'Jordan', 'La politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-06 08:28:00', 2, '2018-06-18 05:51:13'),
(45, 'Bryan', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, bonne année.', '2018-06-01 14:18:00', 1, '2018-06-18 05:51:39'),
(46, 'Kevin', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-14 21:09:00', 4, '2018-06-18 05:50:41'),
(47, 'Tangui', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-04 06:59:00', 5, '2018-06-18 05:51:26'),
(48, 'Bertrand', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-07 10:08:00', 2, '2018-06-18 05:50:55'),
(49, 'Hubert', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-01 09:20:00', 8, '2018-06-18 05:52:31'),
(50, 'Gérard', 'On ne peut pas parler de politique administrative scientifique, la politique à l\'égard de la complexité nous incite à propulser une discipline axé(e)(s) sur la réalité du terrain, je vous en prie.', '2018-06-20 09:22:00', 2, '2018-06-18 05:49:43');

-- --------------------------------------------------------

--
-- Structure de la table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9),
(79, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(80, 15, 'titre', 'text', 'Titre', 1, 1, 1, 1, 1, 1, NULL, 2),
(81, 15, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, NULL, 3),
(82, 15, 'taille', 'text', 'Taille', 1, 1, 1, 1, 1, 1, NULL, 4),
(83, 15, 'image', 'text', 'Image', 1, 1, 1, 1, 1, 1, NULL, 5),
(84, 15, 'categories_id', 'text', 'Categories Id', 1, 1, 1, 1, 1, 1, NULL, 6),
(85, 15, 'fichier', 'text', 'Fichier', 1, 1, 1, 1, 1, 1, NULL, 7),
(86, 15, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, NULL, 8),
(87, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 9),
(88, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 10),
(89, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(90, 16, 'nom', 'text', 'Nom', 1, 1, 1, 1, 1, 1, NULL, 2),
(91, 16, 'image', 'text', 'Image', 1, 1, 1, 1, 1, 1, NULL, 3),
(92, 16, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, NULL, 4),
(93, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(94, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(95, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(96, 17, 'nom', 'text', 'Nom', 1, 1, 1, 1, 1, 1, NULL, 2),
(97, 17, 'texte', 'text', 'Texte', 1, 1, 1, 1, 1, 1, NULL, 3),
(98, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(99, 17, 'projets_id', 'text', 'Projets Id', 1, 1, 1, 1, 1, 1, NULL, 5),
(100, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(101, 15, 'projet_belongsto_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Categorie\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"categories_id\",\"key\":\"id\",\"label\":\"nom\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":null}', 11),
(102, 17, 'commentaire_belongsto_projet_relationship', 'relationship', 'projets', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Projet\",\"table\":\"projets\",\"type\":\"belongsTo\",\"column\":\"projets_id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":null}', 7);

-- --------------------------------------------------------

--
-- Structure de la table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-06-07 10:12:54', '2018-06-07 10:12:54'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-06-07 10:12:54', '2018-06-07 10:12:54'),
(15, 'projets', 'projets', 'Projet', 'Projets', NULL, 'App\\Projet', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-06-20 09:46:51', '2018-06-20 09:46:51'),
(16, 'categories', 'categories', 'Category', 'Categories', NULL, 'App\\Categorie', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-06-20 09:47:04', '2018-06-20 09:47:04'),
(17, 'commentaires', 'commentaires', 'Commentaire', 'Commentaires', NULL, 'App\\Commentaire', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-06-20 09:47:13', '2018-06-20 09:47:13');

-- --------------------------------------------------------

--
-- Structure de la table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-06-07 10:12:56', '2018-06-07 10:12:56');

-- --------------------------------------------------------

--
-- Structure de la table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2018-06-07 10:12:56', '2018-06-07 10:12:56', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2018-06-07 10:12:56', '2018-06-07 10:12:56', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2018-06-07 10:12:59', '2018-06-07 10:12:59', 'voyager.hooks', NULL),
(12, 1, 'Categories', '', '_self', NULL, NULL, NULL, 15, '2018-06-11 06:35:48', '2018-06-11 06:35:48', 'voyager.categories.index', NULL),
(13, 1, 'Posts', '', '_self', NULL, NULL, NULL, 16, '2018-06-11 06:36:09', '2018-06-11 06:36:09', 'voyager.posts.index', NULL),
(14, 1, 'Post Has Categories', '', '_self', NULL, NULL, NULL, 17, '2018-06-11 06:36:39', '2018-06-11 06:36:39', 'voyager.posts-has-categories.index', NULL),
(15, 1, 'Commentaires', '', '_self', NULL, NULL, NULL, 18, '2018-06-11 06:53:38', '2018-06-11 06:53:38', 'voyager.commentaires.index', NULL),
(16, 1, 'Post Has Commentaires', '', '_self', NULL, NULL, NULL, 19, '2018-06-11 06:55:30', '2018-06-11 06:55:30', 'voyager.posts-has-commentaires.index', NULL),
(17, 1, 'Projets', '', '_self', NULL, NULL, NULL, 20, '2018-06-20 09:46:51', '2018-06-20 09:46:51', 'voyager.projets.index', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(2, 'browse_bread', NULL, '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(3, 'browse_database', NULL, '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(4, 'browse_media', NULL, '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(5, 'browse_compass', NULL, '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(11, 'browse_roles', 'roles', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(12, 'read_roles', 'roles', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(13, 'edit_roles', 'roles', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(14, 'add_roles', 'roles', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(15, 'delete_roles', 'roles', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(16, 'browse_users', 'users', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(17, 'read_users', 'users', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(18, 'edit_users', 'users', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(19, 'add_users', 'users', '2018-06-07 10:12:57', '2018-06-07 10:12:57'),
(20, 'delete_users', 'users', '2018-06-07 10:12:58', '2018-06-07 10:12:58'),
(21, 'browse_settings', 'settings', '2018-06-07 10:12:58', '2018-06-07 10:12:58'),
(22, 'read_settings', 'settings', '2018-06-07 10:12:58', '2018-06-07 10:12:58'),
(23, 'edit_settings', 'settings', '2018-06-07 10:12:58', '2018-06-07 10:12:58'),
(24, 'add_settings', 'settings', '2018-06-07 10:12:58', '2018-06-07 10:12:58'),
(25, 'delete_settings', 'settings', '2018-06-07 10:12:58', '2018-06-07 10:12:58'),
(26, 'browse_hooks', NULL, '2018-06-07 10:13:00', '2018-06-07 10:13:00'),
(67, 'browse_projets', 'projets', '2018-06-20 09:46:51', '2018-06-20 09:46:51'),
(68, 'read_projets', 'projets', '2018-06-20 09:46:51', '2018-06-20 09:46:51'),
(69, 'edit_projets', 'projets', '2018-06-20 09:46:51', '2018-06-20 09:46:51'),
(70, 'add_projets', 'projets', '2018-06-20 09:46:51', '2018-06-20 09:46:51'),
(71, 'delete_projets', 'projets', '2018-06-20 09:46:51', '2018-06-20 09:46:51'),
(72, 'browse_categories', 'categories', '2018-06-20 09:47:04', '2018-06-20 09:47:04'),
(73, 'read_categories', 'categories', '2018-06-20 09:47:04', '2018-06-20 09:47:04'),
(74, 'edit_categories', 'categories', '2018-06-20 09:47:04', '2018-06-20 09:47:04'),
(75, 'add_categories', 'categories', '2018-06-20 09:47:04', '2018-06-20 09:47:04'),
(76, 'delete_categories', 'categories', '2018-06-20 09:47:04', '2018-06-20 09:47:04'),
(77, 'browse_commentaires', 'commentaires', '2018-06-20 09:47:13', '2018-06-20 09:47:13'),
(78, 'read_commentaires', 'commentaires', '2018-06-20 09:47:13', '2018-06-20 09:47:13'),
(79, 'edit_commentaires', 'commentaires', '2018-06-20 09:47:13', '2018-06-20 09:47:13'),
(80, 'add_commentaires', 'commentaires', '2018-06-20 09:47:13', '2018-06-20 09:47:13'),
(81, 'delete_commentaires', 'commentaires', '2018-06-20 09:47:13', '2018-06-20 09:47:13');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1);

-- --------------------------------------------------------

--
-- Structure de la table `projets`
--

DROP TABLE IF EXISTS `projets`;
CREATE TABLE IF NOT EXISTS `projets` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `taille` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `categories_id` int(11) NOT NULL,
  `fichier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `projets`
--

INSERT INTO `projets` (`id`, `titre`, `description`, `taille`, `image`, `categories_id`, `fichier`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'PSD Goodies', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 45, '1.jpg', 5, 'psd_goodies.zip', 'psd_goodies', NULL, NULL),
(2, 'Free Fonts', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 12, '2.jpg', 3, 'free_fonts.zip', 'free_fonts', NULL, NULL),
(3, 'Wordpress theme', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 564, '3.jpg', 4, 'wordpress_theme.zip', 'wordpress_theme', NULL, NULL),
(4, 'Illustrator freebies', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 132, '4.jpg', 6, 'illustrator_freebies.zip', 'illustrator_freebies', NULL, NULL),
(5, 'HTML Theme', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 84, '5.jpg', 4, 'html_theme.zip', 'html_theme', NULL, NULL),
(6, 'Illustrator Icons', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 10, '6.jpg', 6, 'illustrator_icons.zip', 'illustrator_icons', NULL, NULL),
(7, 'PSD Icons', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 9, '7.jpg', 5, 'psd_icons.zip', 'psd_icons', NULL, NULL),
(8, 'Premium UI', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 980, '8.jpg', 1, 'premium_ui.zip', 'premium_ui', NULL, NULL),
(9, 'PSD MockUp', 'D\'une manière ou d\'une autre, la contextualisation par rapport aux diplomaties paraît réglementer les grabuses lastiques dans ces prestances, bonnes fêtes. ', 485, '9.jpg', 5, 'psd_mockup.zip', 'psd_mockup', NULL, NULL),
(10, 'Premium Fonts', 'Tandis que la politique est encadrée par des scientifiques issus de Sciences Po et Administratives, la compétence par rapport aux diplomaties tarde à mettre un accent sur les interchanges vers Lovanium, bonnes fêtes.', 51, '10.jpg', 1, 'premium_fonts.zip', 'premium_fonts', '2018-06-20 09:51:31', '2018-06-20 09:51:31'),
(11, 'Wordpress Fonts', 'Tandis que la politique est encadrée par des scientifiques issus de Sciences Po et Administratives, la compétence par rapport aux diplomaties tarde à mettre un accent sur les interchanges vers Lovanium, bonnes fêtes. ', 15, '11.jpg', 3, 'Wordpress.zip', 'wordpress', '2018-06-20 07:14:15', NULL),
(12, 'Avy tymptir', 'Zyhys onoso jehikagon Aeksiot epi, se gis hen syndrorro jemagon. Toli rhuqo lotinti, kostilus. ', 26, '12.jpg', 5, 'avy_tymptir.zip', 'avy_tymptir', '2018-06-20 05:17:16', NULL),
(13, 'Aena shekhikhi', 'Shekh ma shieraki anni. San athchomari yeraan. Nevakhi vekha ha maan: Rekke, m\'aresakea norethi fitte. ', 15, '13.jpg', 4, 'aena_shekhikhi.zip', 'aena_shekhikhi', '2018-06-19 05:20:28', NULL),
(14, 'Hrakkares athchomarhrakkares', 'Anha zhilak yera norethaan. Yer affesi anni. Hash anha atihak yera save? ', 78, '14.jpg', 3, 'hrakkares_athchomarhrakkares.zip', 'hrakkares_athchomarhrakkares', '2018-06-20 06:21:23', NULL),
(15, 'Hrakkares chomakea', 'Khal ahhas arakh. Jalan atthirari anni. Shekh ma shieraki anni. ', 31, '15.jpg', 2, 'hrakkares_chomakea.zip', 'hrakkares_chomakea', '2018-06-20 11:28:26', NULL),
(16, 'Khal ahhas', 'Fini hazi? Yer affesi anni. Athdavrazar! ', 64, '16.jpg', 1, 'khal_ahhas.zip', 'khal_ahhas', '2018-06-20 12:36:26', NULL),
(17, 'Fini hazi', 'Nevakhi vekha ha maan: Rekke, m\'aresakea norethi fitte. Hash yer dothrae chek asshekh? Jalan atthirari anni. ', 91, '17.jpg', 6, 'fini_hazi.zip', 'fini_hazi', '2018-06-20 07:20:21', NULL),
(18, 'Nevakhi vekha', 'San athchomari yeraan. Jalan atthirari anni. Ezas eshna gech ahilee! San athchomari yeraan. Jalan atthirari anni. Ezas eshna gech ahilee! San athchomari yeraan. Jalan atthirari anni. Ezas eshna gech ahilee! ', 31, '18.jpg', 5, 'nevakhi_vekha.zip', 'nevakhi_vekha', '2018-06-20 12:29:31', NULL),
(19, 'San athchomari', 'Es havazhaan. Fini hazi? Anha zhilak yera norethaan. Anha zhilak yera norethaan. Nevakhi vekha ha maan: Rekke, m\'aresakea norethi fitte. Anha dothrak chek asshekh. Nevakhi vekha ha maan: Rekke, m\'aresakea norethi fitte. Jin ave sekke verven anni m\'orvikoon. Ifas maisi yeri. Anha dothrak chek asshekh. ', 123, '19.jpg', 4, 'san_athchomari.zip', 'san_athchomari', '2018-06-20 07:39:32', NULL),
(20, 'Es havazhaan', 'Ezas eshna gech ahilee! Hash yer dothrae chek asshekh? Ezas eshna gech ahilee! Shekh ma shieraki anni. Athdavrazar! Shekh ma shieraki anni. Anha dothrak chek asshekh. Ifas maisi yeri. Athdavrazar! Jalan atthirari anni. ', 21, '20.jpg', 3, 'es_havazhaan.zip', 'es_havazhaan', '2018-06-20 09:26:16', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-06-07 10:12:56', '2018-06-07 10:12:56'),
(2, 'user', 'Normal User', '2018-06-07 10:12:57', '2018-06-07 10:12:57');

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Structure de la table `translations`
--

DROP TABLE IF EXISTS `translations`;
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'David', 'bidi1609@hotmail.com', 'users/default.png', '$2y$10$j7pfwodK/mQkUMjpM277KO/0T.KX273sS3vetv2kXdnQJh86fwYHu', NULL, NULL, '2018-06-11 05:45:21', '2018-06-11 05:45:22');

-- --------------------------------------------------------

--
-- Structure de la table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(1, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Contraintes pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
